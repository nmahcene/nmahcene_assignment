* Quick summary

This application loads information at startup from CSV file and populate MYSQL Database. No need to create table.

Just follow the steps from Most visited web sites application.docx word file under Config and Documentation  folder.

Execution:

java -jar most-visited.sites-1.0.jar --spring.config.location=c:/dev/config.properties

The jar file is under Config and Documentation.

spring.datasource.url= jdbc:mysql://localhost:3306/world

world is the name of my schema, change to your database schema name.

If you have any questions, do not hesitate please.