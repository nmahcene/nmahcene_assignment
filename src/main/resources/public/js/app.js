var myModuleConfig = function($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise("/");

    $stateProvider.state("home", {
        url: "/",
        templateUrl: "/views/home.html"
    }).state("getWebSites", {
        url: "/view-sites/",
        templateUrl: "/views/view-sites.html",
        controller: "viewSitesController"
    }).state("getByDate", {
        url: "edit/:cityCode",
        templateUrl: "/views/admin/cities/edit.html",
        controller: "adminSitesEditController"
    });
};

myModuleConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

// Services

var sitesService = function($resource){
    return $resource("/visited/:id", {
        id: "@visitdate"
    }, {
    	GetMostVisitedWebSites_byDate: {
            method: "GET"
        }
    });
};
sitesService.$inject = ["$resource"];

// Controllers
var viewSitesController = function($scope, sitesService){
    $scope.sites = sitesService.query();
    $scope.filterText = "";
};

viewSitesController.$inject = ["$scope", "sitesService"];

var adminSitesEditController = function($scope, sitesService, $state, $stateParams){
    $scope.city = sitesService.get({id: $stateParams.cityCode});

    $scope.update = function(){
        $scope.city.$update(function(){
            $state.go("admin.cities", {}, {reload: true});
        });
    };
};


adminSitesEditController.$inject = ["$scope", "sitesService", "$state", "$stateParams"];


// Init module
angular.module("myFirstAngularModule", ["ui.router", "ngResource"])
    .config(myModuleConfig)
    .factory("sitesService", sitesService)
    .controller("viewSitesController", viewSitesController)
    .controller("adminSitesEditController", adminSitesEditController);
    