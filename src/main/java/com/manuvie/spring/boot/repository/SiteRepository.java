package com.manuvie.spring.boot.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.manuvie.spring.boot.model.Site;


@Repository
public interface SiteRepository extends CrudRepository<Site,String> {

	 List<Site> findByVisitdate(String visitdate);
	 List<Site> findAll();
}
