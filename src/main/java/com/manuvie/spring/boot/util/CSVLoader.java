package com.manuvie.spring.boot.util;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.csvreader.CsvReader;
import com.manuvie.spring.boot.model.Site;
import com.manuvie.spring.boot.repository.SiteRepository;


@Component
public class CSVLoader {
	
    @Autowired	
	private SiteRepository siteRepository;
    
    @Value("${site.csv.file}")
    private String csvFile;
    
    @Value("${site.csv.separator}")
    private String csvSeparaor;
    
    

    public void loadCSV() {
    	 try{
 				CsvReader sites = new CsvReader(csvFile,csvSeparaor.charAt(1));
 				sites.readHeaders();
				while (sites.readRecord())
				{
					Site site = new Site();
					site.setVisitdate(sites.get("date"));
					site.setName(sites.get("website"));
					site.setNumber(Long.valueOf(sites.get("visits")));
					siteRepository.save(site);
				}
		
				sites.close();
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
}
