package com.manuvie.spring.boot.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.manuvie.spring.boot.model.Result;
import com.manuvie.spring.boot.model.Site;
import com.manuvie.spring.boot.repository.SiteRepository;

@Component
public class VisitServiceImpl implements VisitService {

    @Autowired	
	private SiteRepository siteRepository;
    
    @Value("${limit.number}")
    private int limit;
	
	//Return limit of "limit variable" set in config.properties
    @Override
	public List<Site> getMostVisitedSites(String visitdate) {
		
		List<Site> websites = siteRepository.findByVisitdate(visitdate);
		return websites.stream()  
	       .sorted((e1,e2) -> Long.compare(e2.getNumber(),e1.getNumber()))
	       .limit(limit).collect(Collectors.toList());
	}


	@Override
	public List<Site> getAllSites() {
		List<Site> websites =  siteRepository.findAll();
		return websites.stream()  
		       .sorted((e1,e2) -> e1.getName().compareTo(e2.getName()))
			   .collect(Collectors.toList());
	}


	@Override
	public List<Site> getAllSitesByName(String name) {
		List<Site> websites =  siteRepository.findAll();
		return websites.stream().
				   filter(e -> e.getName().contains(name))
			 	   .collect(Collectors.toList());
	}


	@Override
	public Result sumSitesByName(String name) {
		List<Site> websites =  getAllSitesByName(name);
		Long total = websites.stream().
				   filter(e -> e.getNumber() > 0 ).mapToLong(Site::getNumber).sum();
		
		String siteName = websites.stream().map(Site::getName).findAny().get();
				   
		
		Result result = new Result();
		result.setSite_name(siteName);
		result.setTotal(total);
		return result;
	}
}
