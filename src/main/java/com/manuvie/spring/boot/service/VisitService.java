package com.manuvie.spring.boot.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.manuvie.spring.boot.model.Result;
import com.manuvie.spring.boot.model.Site;

@Service
public interface VisitService {
	public List<Site> getMostVisitedSites(String visitdate);
	public List<Site> getAllSites();
	public List<Site> getAllSitesByName(String name);
	public Result sumSitesByName(String name);
}
