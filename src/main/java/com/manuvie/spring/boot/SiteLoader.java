package com.manuvie.spring.boot;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.manuvie.spring.boot.util.CSVLoader;

@Component
public class SiteLoader implements ApplicationListener<ContextRefreshedEvent> {
	
	private Logger log = Logger.getLogger(SiteLoader.class);
	
	@Autowired
	CSVLoader csvLoader;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		csvLoader.loadCSV();
	}
}
