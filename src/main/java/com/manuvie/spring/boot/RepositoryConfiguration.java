package com.manuvie.spring.boot;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@EntityScan(basePackages = {"com.manuvie.spring.boot.model"})
@EnableJpaRepositories(basePackages = {"com.manuvie.spring.boot.repository"})
public class RepositoryConfiguration {

}
