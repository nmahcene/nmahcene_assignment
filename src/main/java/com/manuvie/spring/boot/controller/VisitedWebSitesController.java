package com.manuvie.spring.boot.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manuvie.spring.boot.model.Result;
import com.manuvie.spring.boot.model.Site;
import com.manuvie.spring.boot.service.VisitService;

@RestController
@RequestMapping("/visited")
public class VisitedWebSitesController {


	@Autowired
	VisitService visitService;
	
	
	//Getting web sites by date
	@RequestMapping(method=RequestMethod.GET, value="/{visitdate}" )
	public List<Site> getMostVisitedWebSites_byDate(@PathVariable String visitdate) {
		List<Site> sites = visitService.getMostVisitedSites(visitdate);
		return sites;
	}
	
	
	
	//Getting web sites by name
	@RequestMapping(method=RequestMethod.GET, value="/name/{name}" )
	public List<Site> getMostVisitedWebSites_byName(@PathVariable String name) {
		List<Site> sites = visitService.getAllSitesByName(name);
		return sites;
	}
	
	
	//Sum number of visit on specific name
	@RequestMapping(method=RequestMethod.GET, value="/name/{name}/sum" )
	public Result getSumOfVisitedWebSites_byName(@PathVariable String name) {
		Result result = visitService.sumSitesByName(name);
		return result;
	}
	
	
	//Getting all web sites
	@RequestMapping(method=RequestMethod.GET )
	public List<Site> getMostVisitedWebSites() {
		return visitService.getAllSites();
	}
	
}
