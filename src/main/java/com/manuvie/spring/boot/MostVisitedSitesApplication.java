package com.manuvie.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MostVisitedSitesApplication {

	public static void main(String[] args) {
		SpringApplication.run(MostVisitedSitesApplication.class, args);
	}
}
