package com.manuvie.spring.boot.model;

public class Result {
	
	private String site_name;
	private long total;

	public String getSite_name() {
		return site_name;
	}
	public void setSite_name(String site_name) {
		this.site_name = site_name;
	}

	

	public long getTotal() {
		return total;
		
	}
	public void setTotal(long total) {
		this.total = total;
	}

}
