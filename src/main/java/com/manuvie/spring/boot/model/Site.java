package com.manuvie.spring.boot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Site {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	

	private String visitdate;
	private String name;
	private long number;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}


	public String getVisitdate() {
		return visitdate;
	}
	
	
	public void setVisitdate(String visitdate) {
		this.visitdate = visitdate;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getNumber() {
		return number;
	}
	public void setNumber(long number) {
		this.number = number;
	}
	
		
	@Override
	public String toString() {
		return "WebSite [visitdate=" + visitdate + ", name=" + name + ", number=" + number + "]";
	}
}
